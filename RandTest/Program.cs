﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace RandTest
{
    public class Goods
    {

        public string Name { get; set; }

        public int StockNumber { get; set; }
    }

    class Program
    {
        static void Main(string[] args)
        {
           

            //开奖次数
            var loopCount = 100;//goodsList.Sum(t => t.StockNumber);

            //loopCount = loopCount < openCount ? loopCount : openCount;
            var rand = new Random((int)DateTime.Now.Ticks);
            
            for(var n = 0; n < 10; ++n)
            {
                Console.WriteLine($"-----第{n+1}次测试----------------");
                
                List<Goods> goodsList = new List<Goods>() {
                    new Goods(){ Name="小熊涮烤一体锅",StockNumber=1 },
                    new Goods(){ Name="usmile电动牙刷",StockNumber=1 },
                    new Goods(){ Name="金稻纳米喷雾补水加湿蒸脸仪",StockNumber=1 },
                    new Goods(){ Name="鳄鱼龙龙（特大娃娃）",StockNumber=1000 },
                    new Goods(){ Name="膳魔师保温杯男女JNL502",StockNumber=19 },
                    new Goods(){ Name="飞利浦电动剃须刀S526",StockNumber=1 },
                };

                var statisticaDic = new Dictionary<Goods, int>();
                goodsList.ForEach(goods => statisticaDic.Add(goods, 0));

                for (var i = 0; i < loopCount; ++i)
                {
                    var allGiftItemList = new List<Goods>();
                    goodsList.ToList().ForEach(item => {
                        for (var i = 0; i < item.StockNumber; ++i)
                        {
                            allGiftItemList.Add(item);
                        }
                    });

                    var r = rand.Next(0, allGiftItemList.Count - 1);
                    var gift = allGiftItemList[r];
                    gift.StockNumber--;
                    statisticaDic[gift]++;
                }
                Console.WriteLine($"共开奖{loopCount}次，开奖情况：");
                foreach (var kv in statisticaDic)
                {
                    Console.WriteLine($"{kv.Key.Name} 中奖几率：{Math.Round(((decimal)kv.Value / loopCount) * 100, 2)}%");
                }
            }

           
        }
    }
}
